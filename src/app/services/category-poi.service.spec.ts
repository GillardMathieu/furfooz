import { TestBed } from '@angular/core/testing';

import { CategoryPOIService } from './category-poi.service';

describe('CategoryPOIService', () => {
  let service: CategoryPOIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CategoryPOIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
