import { TestBed } from '@angular/core/testing';

import { PointsofinterestService } from './pointsofinterest.service';

describe('PointsofinterestService', () => {
  let service: PointsofinterestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PointsofinterestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
