import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { poiObject } from '../models/poi';

@Injectable({
  providedIn: 'root'
})
export class PointsofinterestService {

  // url: string = "http://10.10.2.53/ApiFurfooz/api/pointinteret";
  url: string = "https://furfooz.somee.com/api/pointinteret";

  context:BehaviorSubject<poiObject[]>

  constructor(private client : HttpClient) {
    this.context = new BehaviorSubject<poiObject[]>([]);
            this.refresh();
   }
   refresh() {
    this.client.get<poiObject[]>(this.url).pipe(map((x) => {
      for(let item of x)
      {
        item.IsSee = false;
        item.AlreadyShown = false;
        item.CurrentPositionDist = 0;
      }
      return x;
    })).subscribe(data => this.context.next(data));
  }
}
