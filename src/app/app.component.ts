import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { ModalController } from '@ionic/angular';
import { PoiListPage } from './poi-list/poi-list.page';

import {TranslateService} from '@ngx-translate/core';
import { SettingsPage } from './settings/settings.page';
import { environment } from 'src/environments/environment';

import { Storage } from '@ionic/storage';
import { CategoryPOIService } from './services/category-poi.service';
import { CategoryPoi } from './models/categoryPOI';
import { ListpoicatPage } from './listpoicat/listpoicat.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  CategoryPoiList: CategoryPoi[];
  error : any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public modalController: ModalController,
    public translate: TranslateService,
    private storage: Storage,
    private categoryPoiService: CategoryPOIService
  ) {
    this.initializeApp();
    translate.addLangs(['en', 'fr', 'nl']);
    translate.setDefaultLang('en');
    storage.get('selectedLanguages').then((x) => {translate.use(x)});

    this.categoryPoiService.context.subscribe(data => {
      this.CategoryPoiList = data; this.error = data
    }, error => alert(JSON.stringify(error)))

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async poiModal() {
    const modal = await this.modalController.create({
      component: PoiListPage,
      mode: 'ios',
      cssClass: 'my-custom-class',
      swipeToClose: true
    });
    return await modal.present();
  }

  async settingsModal() {
    const modal = await this.modalController.create({
      component: SettingsPage,
      mode: 'ios',
      cssClass: 'my-custom-class',
      swipeToClose: true
    });
    return await modal.present();
  }

  async listpoiModal(id:number) {
    const modal = await this.modalController.create({
      component: ListpoicatPage,
      mode: 'ios',
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {'IdCat' : id}
    });
    return await modal.present();
  }
}
