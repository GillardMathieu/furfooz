import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListpoicatPage } from './listpoicat.page';

const routes: Routes = [
  {
    path: '',
    component: ListpoicatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListpoicatPageRoutingModule {}
