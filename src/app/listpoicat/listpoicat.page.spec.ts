import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListpoicatPage } from './listpoicat.page';

describe('ListpoicatPage', () => {
  let component: ListpoicatPage;
  let fixture: ComponentFixture<ListpoicatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListpoicatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListpoicatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
