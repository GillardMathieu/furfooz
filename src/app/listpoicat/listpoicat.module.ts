import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListpoicatPageRoutingModule } from './listpoicat-routing.module';

import { ListpoicatPage } from './listpoicat.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListpoicatPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [ListpoicatPage]
})
export class ListpoicatPageModule {}
