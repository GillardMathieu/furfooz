import { Component, AfterContentInit, ChangeDetectorRef, EventEmitter } from '@angular/core';
import{Map, tileLayer, marker, polyline, Icon } from "leaflet";
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import L from "leaflet";
import 'node_modules/leaflet-gpx';
import "leaflet/dist/images/marker-icon-2x.png";
import "leaflet/dist/images/marker-shadow.png";
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AlertController } from '@ionic/angular';
import { RouterLink, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalPagePage } from '../modal-page/modal-page.page';
import { PointsofinterestService } from '../services/pointsofinterest.service';
import { poiObject } from '../models/poi';
import { GenericpoiPage } from '../genericpoi/genericpoi.page';
import { CategoryPoi } from '../models/categoryPOI';
import { CategoryPOIService } from '../services/category-poi.service';
import { Storage } from '@ionic/storage';

declare var window;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements AfterContentInit {
  marker : marker;
  map: L.Map;
  latLong = []; 
  position: any;
  myLat: any;
  myLong: any;

  CategoryPoiList: CategoryPoi[];

  PoiList: poiObject[];

  rl : RouterLink;
  units : string;

  todayDay : number;
  todayMonth: number;

  constructor(
    private geolocation: Geolocation,
    private detectChangeRef: ChangeDetectorRef,
    private localNotifications: LocalNotifications,
    private alertCtrl: AlertController,
    private router: Router,
    public modalController: ModalController,
    private poiService: PointsofinterestService,
    private catService : CategoryPOIService,
    private storage: Storage,
    private alertController: AlertController
  ) {
    window.map = this;
    this.todayDay = new Date().getDate();
    this.todayMonth = new Date().getMonth();
  }
  
  
  showMap(){

    
    let mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWF0aGlldWdpbGxhcmQiLCJhIjoiY2tmY2lkZHhwMTBuczMybzhseW42d3R0NSJ9.Xw8nn6zfWoCZtuK15d0V3w';
    
    let attribute = '&copy; <a href="https://www.mapbox.com">MapBox</a> contributors'

    let OutDoors = L.tileLayer(mbUrl, {id : "mapbox/outdoors-v11",
      detectRetina: true,
      attribution: attribute
    });

    let Satellite = L.tileLayer(mbUrl, { id : "mapbox/satellite-v9",
      detectRetina: true,
      attribution: attribute
    });
    
    let Streets = L.tileLayer(mbUrl, { id : "mapbox/streets-v11",
      detectRetina: true,
      attribution: attribute
    });

    let baseLayers = {
      'Outdoor': OutDoors,
      'Satellite': Satellite,
      'Streets' : Streets
    }

    let markers : any = {};
    
    let promise = new Promise((resolve, reject) => {
      this.catService.context.subscribe(catdata => {this.CategoryPoiList = catdata
        if(catdata.length == 0) return
        
        this.poiService.context.subscribe(poidata => {this.PoiList = poidata
          if(poidata.length == 0) return
          
          for(let cat of this.CategoryPoiList)
          {
            let list = [];
            for(let p of this.PoiList.filter(x=> x.Category_id == cat.Id))
            {
              let poiDayStart = p.StartDate != null ? new Date(p.StartDate).getDate() : 1;
              let poiDayEnd = p.EndDate != null ? new Date(p.EndDate).getDate() : 31;
              let poiMonthStart = p.StartDate != null ? new Date(p.StartDate).getMonth() : 0;
              let poiMonthEnd = p.EndDate != null ? new Date(p.EndDate).getMonth() : 11;

              //console.log(`today : ${this.todayDay}-${this.todayMonth} && ${poiDayStart}-${poiMonthStart}/${poiDayEnd}-${poiMonthEnd}`);
              
              
              if(p.Latitude != null 
                && (this.todayDay >= poiDayStart && this.todayMonth >= poiMonthStart && this.todayDay <= poiDayEnd && this.todayMonth <= poiMonthEnd))
                {
                let myIcon = new Icon(
                  {
                    iconUrl: `assets/markers/marker-icon-${cat.PinColor}.png`,
                    shadowUrl: 'assets/markers/marker-shadow.png',
                    iconSize: [25, 41],
                    iconAnchor: [12, 41],
                    shadowSize: [41, 41]
                  }
                )

                let _marker = L.marker([p.Latitude,p.Longitude], {icon: myIcon}).bindPopup(p.Name_fr);
                
                list.push(_marker);
              }
            }
            
            if(this.PoiList.filter(x=> x.Category_id == cat.Id && x.Latitude!= null).length > 0)
            {
              markers[cat.Name_fr] = L.layerGroup(list);
            }
            
          }
        resolve();
        })
      })
    }).then( () => {
      
      this.map = L.map('myMap', {
        center : [50.2132,4.9540],
        zoom : 15,
        layers : [ OutDoors],
        minZoom : 15,
        maxZoom : 17
      })
  
      let southWest = L.latLng([50.2075, 4.9372]);
      let northEast = L.latLng([50.2188, 4.9681]);
      
      let bounds = L.latLngBounds(southWest, northEast);
      // var gpx = 'assets/gpx/testmouton.gpx' // URL to your GPX file or the GPX itself
      // new L.GPX(gpx, 
      //   {
      //     async: true,
      //     marker_options: {
      //       startIconUrl: '',
      //       endIconUrl: '',
      //       shadowUrl: '' 
      //     },
      //     polyline_options: {
      //       color: 'green',
      //       opacity: 0.75,
      //       weight: 3,
      //       lineCap: 'round'
      //     }
      //   }).on('loaded', function(e) {
      // this.map.fitBounds(e.target.getBounds());  
      // }).addTo(this.map);

      // var hexa = L.polygon([
      //   [50.212702,4.952329],
      //   [50.212251,4.952299],
      //   [50.212236,4.953004],
      //   [50.212683,4.953027],
      //   [50.212702,4.952329]
			// ], {color: 'red'}).addTo(this.map);
      
      this.map.setMaxBounds(bounds);

      L.control.layers(baseLayers ,markers).addTo(this.map);
      L.control.scale().addTo(this.map);

      this.getPositions();
    })

    
  }
  
  ngAfterContentInit() {
    this.units = 'km';
    // setTimeout(() => { this.map.invalidateSize(true); }, 500);
    setTimeout(() => { }, 500);
    this.showMap();
  }
  
  
  ionViewDidLeave() {
    //this.showMap();
  }
  getPositions() {
    
    this.storage.get('units').then((x) => {this.units = x});
    this.geolocation.watchPosition({
      enableHighAccuracy : true,
      timeout: 500,
      maximumAge: 2000
    }).subscribe(position => {
      if(position!=null)
      {
        this.detectChangeRef.detectChanges();
        position = (position as Geoposition);
        this.myLat = position.coords.latitude;
        this.myLong = position.coords.longitude;
        for(let e of this.PoiList)
        {
          let eventLocation = { lat: e.Latitude, lng: e.Longitude };
          let myLocation = { lat: position.coords.latitude, lng: position.coords.longitude };

          let dist = this.getDistanceBetweenPoints(eventLocation, myLocation, this.units);

          if(e.Latitude != null)
          {
            e.CurrentPositionDist = dist;
          }
          else
          {
            e.CurrentPositionDist = 0;
          }

          if(e.AlreadyShown==false)
          {
            if(dist < e.Interval && e.IsSee == false)
            // if(dist < 16 && e.IsSee == false)
            {
              let alert = this.alertCtrl.create({
                message: `Voulez vous voir ${e.Name_fr} ?`,
                buttons: [
                  {
                    text: 'Non',
                    role: 'cancel',
                    handler: () => {
                    }
                  },
                  {
                    text: 'Oui',
                    handler: () => {
                      e.IsSee = true;
                      this.poiModal(e.Id);
                    }
                  }
                ]
              });
              alert.then(a => {a.present()});
            }
            e.AlreadyShown = true;
          }
        }
        this.position = position;
        this.latLong = [ 
          position.coords.latitude,
          position.coords.longitude
        ];

        this.showMarker(this.latLong)
      }
    },(e) => this.position = "cannot find position")
  }
  showMarker(latLong){

    let currentIcon = new Icon(
      {
        iconUrl: 'assets/markers/marker-icon-red.png',
        shadowUrl: 'assets/markers/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        shadowSize: [41, 41]
      }
    )

    if(this.marker == null) {
      this.marker = marker(this.latLong, {icon: currentIcon});
      this.marker.addTo(this.map)  
    }
    else{
      this.marker.setLatLng(this.latLong);
    }
  }

  getDistanceBetweenPoints(start, end, units){

    let earthRadius = {
        miles: 3958.8,
        km: 6371
    };

    let R = earthRadius[units || 'km'];
    let lat1 = start.lat;
    let lon1 = start.lng;
    let lat2 = end.lat;
    let lon2 = end.lng;

    let dLat = this.toRad((lat2 - lat1));
    let dLon = this.toRad((lon2 - lon1));
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
    Math.sin(dLon / 2) *
    Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;

    return d;

  }

  toRad(x){
    return x * Math.PI / 180;
  }
  setPosition() {
    this.map.panTo([this.myLat, this.myLong]);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPagePage,
      mode: 'ios',
      swipeToClose: true,
      componentProps: {'Eventlist' : this.PoiList.filter(x => x.CurrentPositionDist > 0)}
    });
    return await modal.present();
  }

  async poiModal(id:number) {
    const modal = await this.modalController.create({
      component: GenericpoiPage,
      mode: 'ios',
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {'IdPOI' : id}
    });
    return await modal.present();
  }

  async refreshDistance () {
    
    this.units = await this.storage.get('units');
    console.log(this.units);
    
    for(let e of this.PoiList)
        {
          let eventLocation = { lat: e.Latitude, lng: e.Longitude };
          let myLocation = { lat: this.myLat, lng: this.myLong };

          let dist = this.getDistanceBetweenPoints(eventLocation, myLocation, this.units);

          if(e.Latitude != null)
          {
            e.CurrentPositionDist = dist;
          }
          else
          {
            e.CurrentPositionDist = 0;
          }
        }
  }

  async ShowLegend() {

    let rowMessage = [];

    for(let cat of this.CategoryPoiList)
    {
      rowMessage.push(`<div>
                        <img src="assets/markers/marker-icon-${cat.PinColor}.png" alt="" style="padding-right: 10px;">
                        <label>${cat.Name_fr}</label>
                      </div>`);
    }


    const alert = await this.alertController.create({
      header: 'Legend',
      message: rowMessage.join('<br>')
    });

    await alert.present();
  }
}