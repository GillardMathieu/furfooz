import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { poiObject } from '../models/poi';

import { Storage } from '@ionic/storage';
import { GenericpoiPage } from '../genericpoi/genericpoi.page';

@Component({
  selector: 'app-modal-page',
  templateUrl: './modal-page.page.html',
  styleUrls: ['./modal-page.page.scss'],
})
export class ModalPagePage implements OnInit {

  @Input() Eventlist: poiObject[];

  selectedLanguage : string;
  units : string;

  today : Date;
  year: number;

  constructor(
      private modalcontroler : ModalController,
      private storage: Storage
    ) {
        this.today = new Date();
        this.year = new Date().getFullYear();
     }

  ngOnInit() {
    this.storage.get('selectedLanguages').then((x) => {this.selectedLanguage = x});
    this.storage.get('units').then((x) => {this.units = x});
    let toDelete = [];
    for (let index = 0; index < this.Eventlist.length; index++) {
      let poiDayStart = this.Eventlist[index].StartDate != null ? new Date(this.Eventlist[index].StartDate).getDate() : 1;
      let poiDayEnd = this.Eventlist[index].EndDate != null ? new Date(this.Eventlist[index].EndDate).getDate() : 31;
      let poiMonthStart = this.Eventlist[index].StartDate != null ? new Date(this.Eventlist[index].StartDate).getMonth() : 0;
      let poiMonthEnd = this.Eventlist[index].EndDate != null ? new Date(this.Eventlist[index].EndDate).getMonth() : 11;
      // console.log(`today : ${this.todayDay}-${this.todayMonth} && ${poiDayStart}-${poiMonthStart}/${poiDayEnd}-${poiMonthEnd}`);

      let startDate = new Date(this.year, poiMonthStart, poiDayStart);
      let endDate = new Date(this.year, poiMonthEnd, poiDayEnd);
      
      if(this.today < startDate || this.today > endDate)
      {
        toDelete.push(this.Eventlist[index])
      }
    }
    
    for(let poi of toDelete)
    {
      this.Eventlist = this.Eventlist.filter(x => x != poi);
    }

    this.listSort()
  }
  
  close() {
    this.modalcontroler.dismiss();
  }
  
  listSort() {
    this.Eventlist = this.Eventlist.filter(x => x.IsDeleted == false);
    this.Eventlist.sort((a,b) => a.CurrentPositionDist > b.CurrentPositionDist ? 1 : -1);
  }

  getName (e: poiObject) : string  {
    return this.selectedLanguage == 'fr' ? e.Name_fr : this.selectedLanguage == 'en' ? e.Name_en : e.Name_nl
  }

  async poiModal(id:number) {
    const modal = await this.modalcontroler.create({
      component: GenericpoiPage,
      mode: 'ios',
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {'IdPOI' : id}
    });
    return await modal.present();
  }

}
