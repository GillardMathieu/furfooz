import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalPagePageRoutingModule } from './modal-page-routing.module';

import { ModalPagePage } from './modal-page.page';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    ModalPagePageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [ModalPagePage]
})
export class ModalPagePageModule {}
