import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PoiListPageRoutingModule } from './poi-list-routing.module';

import { PoiListPage } from './poi-list.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PoiListPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [PoiListPage]
})
export class PoiListPageModule {}
