import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PoiListPage } from './poi-list.page';

describe('PoiListPage', () => {
  let component: PoiListPage;
  let fixture: ComponentFixture<PoiListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoiListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PoiListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
