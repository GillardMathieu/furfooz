import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { GenericpoiPage } from '../genericpoi/genericpoi.page';

@Component({
  selector: 'app-poi-list',
  templateUrl: './poi-list.page.html',
  styleUrls: ['./poi-list.page.scss'],
})
export class PoiListPage implements OnInit {

  constructor(private modalcontroler : ModalController) { }

  ngOnInit() {
  }

  close() {
    this.modalcontroler.dismiss();
  }
}
